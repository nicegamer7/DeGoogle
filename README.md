# **DeGoogle**
## Description
Google? A Magisk module, which removes, Google?
## Changelog
### V1
Initial Release.
## Requirements
Either Magisk and/or TWRP for a systemless install, or TWRP for a system install.
## Instructions
Install via Magisk Manager or TWRP, thanks to Unity by @ahrion and @Zackptg5.
## Links
[Module XDA Forum Thread](https://forum.xda-developers.com/apps/magisk/module-url-here "Module official XDA thread")

[Latest stable Magisk](http://www.tiny.cc/latestmagisk)
