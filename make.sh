#!/bin/bash
w=gapps
p=open_gapps*.zip
c=${w}/Core
g=${w}/GApps
o=${w}/Optional
s=system
sa=${s}/app
spa=${s}/priv-app
sf=${s}/framework

unzip -qq $p -d $w

find $w -maxdepth 1 -type f \! \( -name gapps-remove.txt -o -name installer.sh \) -delete
rm -rf ${w}/META-INF

lzip -d ${c}/*.lz
for f in ${c}/*.tar
do
	tar -xf $f -C $c
done
rm ${c}/*.tar

lzip -d ${g}/*.lz
for f in ${g}/*.tar
do
        tar -xf $f -C $g
done
rm ${g}/*.tar

lzip -d ${o}/*.lz
for f in ${o}/*.tar
do
        tar -xf $f -C $o
done
rm ${o}/*.tar

cp -rl ${c}/*/*/* $s 2> /dev/null
rm -rf $c
cp -rl ${g}/*/*/* $s 2> /dev/null
rm -rf $g
cp -rl ${o}/*/*/* $s 2> /dev/null
rm -rf $o

for f in ${sa}/*/*
do
	truncate -s 0 $f
	mv -- $f ${f%/*}/.replace
done

for f in ${spa}/*/*
do
	truncate -s 0 $f
	mv -- $f ${f%/*}/.replace
done

for f in ${sf}/*
do
	truncate -s 0 $f
done

sed -i '/^#This file is part of The Open GApps script of @mfonville.$/,/^# Pieces that may be left over from AIO ROMs that can\/will interfere with these GApps$/d' ${w}/installer.sh
sed -i '/^#                                             Installer Error Messages$/,/^exxit 0$/d' ${w}/installer.sh
source ${w}/installer.sh

echo "$remove_list" >> ${w}/gapps-remove.txt
sed -i '/^$/d' ${w}/gapps-remove.txt
rm ${w}/installer.sh

while read -r t
do
	if [[ "." =~ ^(${t:(-4):1}|${t:(-3):1}|${t:(-5):1})$ ]]; then
		m=${t%/*}
		if [ ! -d ${m:1} ]; then
			mkdir -p ${m:1}
		fi
		truncate -s 0 ${t:1}
	else
		if [ ! -f ${t: 1}/.replace ]; then
			rm -rf ${t:1}
			mkdir ${t:1}
			touch ${t:1}/.replace
		fi
	fi
done < ${w}/gapps-remove.txt

rm ${w}/gapps-remove.txt
rmdir ${w}

while read -r e
do
	rm -rf $e
done < exclude
